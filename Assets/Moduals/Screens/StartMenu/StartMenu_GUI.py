# Handels the commands that run from this GUI
from Assets.Moduals.Screens.StartMenu import StartMenu_Commands


class StartMenu:
    '''
    Displays the start menu screen and handles the options and buttons on it
    '''

    def __init__(self, Calendar):
        # Allows access to instances created in the run file
        self.Calendar = Calendar

        # Simplifies some of the names
        self.Main_fr = self.Calendar.Main_fr
        self.TB = self.Calendar.TB

        self.TB.AddFrame(self.Main_fr, Row=0, Column=0)
