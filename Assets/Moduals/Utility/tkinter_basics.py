# Imports pythons tkinter GUI libary
import tkinter as TK
# Imports JSON file handeler
import json
# Import sqlite database handeler
import sqlite3 as lite

# Excpetion raise if any paramiters in calling the Basics class are invalid


class AccessError(Exception):
    '''
    A custom Exception which tell the user if they have called the basics class with incorrect paraniters
    '''

    def __init__(self, message):

        # Calls the Exceptions initialization method
        super().__init__(message)
    
class WidgetError(Exception):
    '''
    A custom Exception which tell the user if they are trying to remove a widget not created in this program
    '''

    def __init__(self):

        # Calls the Exceptions initialization method
        super().__init__("The widget you are trying to remove dosen't exist")

# A program should creates its own instance of this class to use its methods


class Basics:
    '''
    Holds the common tkinter functions that are used often but simplifies them
    All defults colours are handeled automatically
    '''

    '''
    Sets up colours and defults for widgets
    '''

    def __init__(self, root, AccessType=None, JSON_file=None, Database=None, Username=None):
        '''
        The setup method this setsup the defult colours and set up holding lists
        Access Type can be: JSON, DATABASE or defult (none)
        The relevant other options must be given for the Access type chosen
        '''
        # Stores the root window
        self.root = root


        # Sets up the lists to store the tkinter widgets after they are created
        # This helps later with colour changes without frame refesh

        # All widgets
        self.All_Widgets = []
        # Label widgets
        self.All_Labels = []
        self.Pure_Labels = []
        self.Space_Labels = []
        self.Title_Labels = []
        self.SubTitle_Labels = []

        # Button widgets
        self.All_Buttons = []
        self.Pure_Buttons = []
        self.Negetive_Buttons = []
        self.Positive_Buttons = []

        # Entries
        self.All_Entries = []

        # Frames
        self.All_Frames = []

        # Adds the root to the relevant lists
        self.All_Widgets.append(self.root)
        self.All_Frames.append(self.root)

        # Stores a fixed version of the defult defaults
        # These can be changed and will effect all cases where these values are used
        self.FIXED_DEFULTS = {
            "Background": "#7eccf7",
            "Foreground": "Black",
            "Btn_Background": "#2db4ff",
            "Btn_Active": "#2da9ff",
            "QuitBtn_Background": "#ef2804",
            "QuitBtn_Active": "#f52804",
            "PositiveBtn_Background": "#1ece18",
            "PositiveBtn_Active": "#159b11",
            "FontType": "Arial",
            "FontSize": 14,
            "TitleFontSize": 24,
            "SubTitleFontSize": 18
        }

        # The defult defult colours
        self.Defults = {
            "Background": self.FIXED_DEFULTS["Background"],
            "Foreground": self.FIXED_DEFULTS["Foreground"],
            "Btn_Background": self.FIXED_DEFULTS["Btn_Background"],
            "Btn_Active": self.FIXED_DEFULTS["Btn_Active"],
            "QuitBtn_Background": self.FIXED_DEFULTS["QuitBtn_Background"],
            "QuitBtn_Active": self.FIXED_DEFULTS["QuitBtn_Active"],
            "PositiveBtn_Background": self.FIXED_DEFULTS["PositiveBtn_Background"],
            "PositiveBtn_Active": self.FIXED_DEFULTS["PositiveBtn_Active"],
            "FontType":  self.FIXED_DEFULTS["FontType"],
            "FontSize": self.FIXED_DEFULTS["FontSize"],
            "TitleFontSize": self.FIXED_DEFULTS["TitleFontSize"],
            "SubTitleFontSize": self.FIXED_DEFULTS["SubTitleFontSize"]
        }

        # Finds how it will access the defult colours
        if AccessType.lower() == "json":
            # Checks that an accepted json file has been given
            if not JSON_file or "".join(list(JSON_file)[-4:]) != "json":
                raise AccessError("Invalid JSON file")
            # Gets the colours from the json file given
            self.LoadColours_json(JSON_file)
        elif AccessType.lower() == "database":
            if not Database or "".join(list(Database)[-2:]) != "db" or not Username:
                raise AccessError("Invalid database file")
            self.LoadColours_database(Database, Username)
        else:
            self.SetColours()

    def LoadColours_json(self, File):
        '''
        Loads the colours stored in the given json file
        The json file must be layed out with "Defults": ["Colours": [Back...etc]]
        '''

        # Opends the json file to read the colours
        with open(File) as json_file:
            # Loads the relevant section of the json file
            self.ColourData = json.load(json_file)["Defults"]["Colours"]

            # Goes through the data on the json file and stores the data in the defult dictionary
            for data in self.ColourData:
                self.Defults[data] = self.ColourData[data]

        # Sets the dictionary items to colour varables
        self.SetColours()

        return

    def LoadColours_database(self, database, username):
        '''
        Loads the users colours from the database given finding the username
        If no database exists it will create one and set all the defults
        '''

        # Loads the given database
        with lite.connect(database) as self.Con:
            self.Cur = self.Con.cursor()

            # Creates a basic users table ifit doesn't exist
            # This should be overridden by a users achall desired table
            self.Cur.execute(
                "CREATE TABLE IF NOT EXISTS Users (Username PRIMARY KEY)")

            # Creates the colours table if it doesn't exist already
            self.Cur.execute("""CREATE TABLE IF NOT EXISTS Colours (
                    Username TEXT NOT NULL PRIMARY KEY,
                    Background TEXT NOT NULL DEFAULT \"{Background}\",
                    Foreground TEXT NOT NULL DEFAULT \"{Foreground}\",
                    Btn_Background TEXT NOT NULL DEFAULT \"{Btn_Background}\",
                    Btn_Active TEXT NOT NULL DEFAULT \"{Btn_Active}\",
                    QuitBtn_Background TEXT NOT NULL DEFAULT \"{QuitBtn_Background}\",
                    QuitBtn_Active TEXT NOT NULL DEFAULT \"{QuitBtn_Active}\",
                    PositiveBtn_Background TEXT NOT NULL DEFAULT \"{PositiveBtn_Background}\",
                    PositiveBtn_Active NUMERIC NOT NULL DEFAULT \"{PositiveBtn_Active}\",
                    FontType TEXT NOT NULL DEFAULT \"{FontType}\",
                    FontSize INTEGER NOT NULL DEFAULT \"{FontSize}\",
                    TitleFontSize INTEGER NOT NULL DEFAULT \"{TitleFontSize}\",
                    SubTitleFontSize INTEGER NOT NULL DEFAULT \"{SubTitleFontSize}\",
                    FOREIGN KEY (Username) REFERENCES Users (Username) ON UPDATE CASCADE ON DELETE CASCADE
                    );""".format(
                Background=self.FIXED_DEFULTS["Background"],
                Foreground=self.FIXED_DEFULTS["Foreground"],
                Btn_Background=self.FIXED_DEFULTS["Btn_Background"],
                Btn_Active=self.FIXED_DEFULTS["Btn_Active"],
                QuitBtn_Background=self.FIXED_DEFULTS["QuitBtn_Background"],
                QuitBtn_Active=self.FIXED_DEFULTS["QuitBtn_Active"],
                PositiveBtn_Background=self.FIXED_DEFULTS["PositiveBtn_Background"],
                PositiveBtn_Active=self.FIXED_DEFULTS["PositiveBtn_Active"],
                FontType=self.FIXED_DEFULTS["FontType"],
                FontSize=self.FIXED_DEFULTS["FontSize"],
                TitleFontSize=self.FIXED_DEFULTS["TitleFontSize"],
                SubTitleFontSize=self.FIXED_DEFULTS["SubTitleFontSize"]
            )
            )

            # Attempts to insert the new user into the database
            try:
                self.Cur.execute(
                    "INSERT INTO Users (Username) VALUES (?);", (username,))
            # If the username already exists it will exit
            except lite.IntegrityError:
                pass

            # Attempts to inset the new user into the colour database
            try:
                self.Cur.execute(
                    "INSERT INTO Colours (Username) VALUES (?);", (username,))
            # If the username already exists it will pass
            except lite.IntegrityError:
                pass

            # Selects the colours from the database by username
            self.Cur.execute(
                "SELECT * FROM Colours WHERE Username = ?", (username,))

            self.Colour_data = self.Cur.fetchall()[0]

        self.Defults = {
            "Background": self.Colour_data[1],
            "Foreground": self.Colour_data[2],
            "Btn_Background": self.Colour_data[3],
            "Btn_Active": self.Colour_data[4],
            "QuitBtn_Background": self.Colour_data[5],
            "QuitBtn_Active": self.Colour_data[6],
            "PositiveBtn_Background": self.Colour_data[7],
            "PositiveBtn_Active": self.Colour_data[8],
            "FontType":  self.Colour_data[9],
            "FontSize": self.Colour_data[10],
            "TitleFontSize": self.Colour_data[11],
            "SubTitleFontSize": self.Colour_data[12]
        }

        self.SetColours()

    def SetColours(self):
        '''
        Sets the colours from the defult dictionary into useable varables
        '''
        self.Background = self.Defults["Background"]
        self.Foreground = self.Defults["Foreground"]
        self.Btn_Background = self.Defults["Btn_Background"]
        self.Btn_Active = self.Defults["Btn_Active"]
        self.QuitBtn_Background = self.Defults["QuitBtn_Background"]
        self.QuitBtn_Active = self.Defults["QuitBtn_Active"]
        self.PositiveBtn_Background = self.Defults["PositiveBtn_Background"]
        self.PositiveBtn_Active = self.Defults["PositiveBtn_Active"]
        self.Font = (self.Defults["FontType"], self.Defults["FontSize"])
        self.TitleFont = (self.Defults["FontType"],
                          self.Defults["TitleFontSize"])
        self.SubTitleFont = (
            self.Defults["FontType"], self.Defults["SubTitleFontSize"])
        return
    
    def RestoreDefaults(self):
        '''
        Sets all of the colours back to the fixed defaults dictionary
        '''
        self.Defults = self.FIXED_DEFULTS
        self.SetColours()

    '''
    Useful commands for organising the tkinter window and development
    '''

    def WorkInProgress(self):
        '''
        Displays a popup window to say a feature is still a work in progress
        '''
        self.WIP_tl = TK.Toplevel(bg=self.Background)
        self.WIP_lbl = TK.Label(self.WIP_tl, font=self.Font, foreground=self.Foreground, bg=self.Background,
                                text="This feature is currently a work in progress, we appologise fore any inconvinience")
        self.WIP_lbl.grid(row=0, column=0, sticky="nsew")

        self.Align_Grid(self.WIP_tl)

        return

    def Align_Grid(self, Frame):
        '''
        Allows the grids to expand as their frame dose
        '''
        self.Grid_Size = Frame.grid_size()

        # Loops through every column
        for i in range(self.Grid_Size[0]):
            # Sets the weight to a non zero value so it can expand
            Frame.columnconfigure(i, weight=1000)
        # Loops through every row
        for i in range(self.Grid_Size[1]):
            # Sets the weight to a non zero value so it can expand
            Frame.rowconfigure(i, weight=1000)

        return

    def Align_Grid_all(self):
        for frame in self.All_Frames:
            self.Align_Grid(frame)
    
    def RemoveWidget(self, Widget):
        '''
        Deletes a widgets and also removes it from the lists
        '''
        if Widget not in self.All_Widgets:
            raise WidgetError()
        self.GetWidgetType(Widget, Destroy=True)
        Widget.destroy()
    

        
    '''
    Updates the colour values of all widgets attributes
    '''

    def UpdateColours(self, Frame):
        '''
        Updates all of the frames colours
        And the childrens and childrens children etc.
        '''

        # Updates the frames colours first
        self.CallUpdate(Frame)

        # Creats a list of the frames children
        child_list = Frame.winfo_children()

        # Iterates though every child in the list
        for child in child_list:
            # If the child has children
            if child.winfo_children():
                # Adds the children to the list to be iterates though
                child_list.extend(child.winfo_children())

        # Goes through every list item and calls the colour updaters
        for child in child_list:
            self.CallUpdate(child)

    def GetWidgetType(self, Widget, Destroy=False):
        '''
        Returns the type of the widgets
        This is based of the lists that it should be sotred in
        THIS ONLY WORKS FOR WIDGETS CREATED WITH THIS PROGRAM
        '''
        if Destroy:
            self.All_Widgets.remove(Widget)

        if Widget in self.All_Buttons:
            if Destroy:
                self.All_Buttons.remove(Widget)
            if Widget in self.Pure_Buttons:
                if Destroy:
                    self.Pure_Buttons.remove(Widget)
                return "Btn_pure"
            if Widget in self.Positive_Buttons:
                if Destroy:
                    self.Positive_Buttons.remove(Widget)
                return "Btn_positive"
            if Widget in self.Negetive_Buttons:
                if Destroy:
                    self.Negetive_Buttons.remove(Widget)
                return "Btn_negetive"
        if Widget in self.All_Entries:
            if Destroy:
                self.All_Entries.remove(Widget)
            return "Entry"
        if Widget in self.All_Frames:
            if Destroy:
                self.All_Frames.remove(Widget)
            return "Frame"
        if Widget in self.All_Labels:
            if Destroy:
                self.All_Labels.remove(Widget)
            if Widget in self.Pure_Labels:
                if Destroy:
                    self.Pure_Labels.remove(Widget)
                return "Lbl_pure"
            if Widget in self.Space_Labels:
                if Destroy:
                    self.Space_Labels.remove(Widget)
                return "Lbl_spacer"
            if Widget in self.Title_Labels:
                if Destroy:
                    self.Title_Labels.remove(Widget)
                return "Lbl_title"
            if Widget in self.SubTitle_Labels:
                if Destroy:
                    self.SubTitle_Labels.remove(Widget)
                return "Lbl_subtitle"
        return None

    def CallUpdate(self, Widget):
        '''
        Finds the type of the given widget and calls the appropiate update commands
        '''
        Type = self.GetWidgetType(Widget)

        # This will be the same for all widgets
        self.UpdateColours_foreground(Widget)

        # If its a frame only bg needs changing
        if Type == "Frame":
            self.UpdateColours_background(Widget)
        # If its an entry font must change
        elif Type == "Entry":
            self.UpdateColours_font(Widget)
        # Tests if it is label as they all have same background
        elif "Lbl" in Type:
            self.UpdateColours_background(Widget)

            # Checks if it's "pure" or spacer label
            if Type == "Lbl_pure" or Type == "Lbl_spacer":
                self.UpdateColours_font(Widget)
            # Checks if it is title
            elif Type == "Lbl_title":
                self.UpdateColours_titlefont(Widget)
            # Checks if it is subtitle
            elif Type == "Lbl_subtitle":
                self.UpdateColours_subtitlefont(Widget)
        # Tests if it is a button as they share a font
        elif "Btn" in Type:
            self.UpdateColours_font(Widget)
            # Tests if it is normal button
            if Type == "Btn_pure":
                self.UpdateColours_btn_background(Widget)
                self.UpdateColours_btn_active(Widget)
            elif Type == "Btn_positive":
                self.UpdateColours_positivebtn_background(Widget)
                self.UpdateColours_positivebtn_active(Widget)
            elif Type == "Btn_negetive":
                self.UpdateColours_quitbtn_background(Widget)
                self.UpdateColours_quitbtn_active(Widget)

        return

    def UpdateColours_background(self, Frame):
        '''
        Updates the frames background colour
        '''
        Frame.config(bg=self.Background)

        return

    def UpdateColours_foreground(self, Frame):
        '''
        Updates the frames foreground colour
        '''
        Frame.config(fg=self.Foreground)

        return

    def UpdateColours_btn_background(self, Frame):
        '''
        Updates the frames btn_background colour
        '''
        Frame.config(bg=self.Btn_Background)

        return

    def UpdateColours_btn_active(self, Frame):
        '''
        Updates the frames btn_active colour
        '''
        Frame.config(activebackground=self.Btn_Active)

        return

    def UpdateColours_quitbtn_background(self, Frame):
        '''
        Updates the frames quitbtn_background colour
        '''
        Frame.config(bg=self.QuitBtn_Background)

        return

    def UpdateColours_quitbtn_active(self, Frame):
        '''
        Updates the frames quitbtn_active colour
        '''
        Frame.config(activebackground=self.QuitBtn_Active)

        return

    def UpdateColours_positivebtn_background(self, Frame):
        '''
        Updates the frames positivebtn_background colour
        '''
        Frame.config(bg=self.PositiveBtn_Background)

        return

    def UpdateColours_positivebtn_active(self, Frame):
        '''
        Updates the frames positivebtn_active colour
        '''
        Frame.config(activebackground=self.PositiveBtn_Active)

        return

    def UpdateColours_font(self, Frame):
        '''
        Updates the frames font colour
        '''
        Frame.config(font=self.Font)

        return

    def UpdateColours_titlefont(self, Frame):
        '''
        Updates the frames titlefont colour
        '''
        Frame.config(font=self.TitleFont)

        return

    def UpdateColours_subtitlefont(self, Frame):
        '''
        Updates the frames subtitlefont colour
        '''
        Frame.config(font=self.SubTitleFont)

        return


    '''
    The core of the program to simplify standered tkinter settings
    '''

    def AddFrame(self, Frame, Pack=False, Row=None, Column=None, CSpan=1, RSpan=1):
        '''
        Creates a Frame for building the GUI on to
        If pack is true then it will pack it into the given frame
        Row, Column etc. are not needed if packing is True
        '''
        # Creats the frame object
        Frame = TK.Frame(Frame, bg=self.Background)

        # Packs the Frame into the given master
        if Pack:
            Frame.pack(fill=TK.BOTH, expand=True)
        # Puts the frame into the grid
        else:
            Frame.grid(row=Row, column=Column,
                       sticky="nsew", columnspan=CSpan, rowspan=RSpan, padx=2, pady=2)

        self.All_Widgets.append(Frame)
        self.All_Frames.append(Frame)

        return Frame

    def AddLabel(self, Frame, Text, Pack=False, Row=None, Column=None, CSpan=1, RSpan=1):
        '''
        Creates a Label for dispalaying text
        If pack is true then it will pack it into the given frame
        Row, Column etc. are not needed if packing is True
        '''
        # Creats the Label object
        Label = TK.Label(Frame, bg=self.Background,
                         fg=self.Foreground, font=self.Font, text=Text)

        # Packs the Label into the given master
        if Pack:
            Label.pack(fill=TK.BOTH, expand=True)
        # Puts the Label into the grid
        else:
            Label.grid(row=Row, column=Column,
                       sticky="nsew", columnspan=CSpan, rowspan=RSpan, padx=2, pady=2)

        self.All_Widgets.append(Label)
        self.All_Labels.append(Label)
        self.Pure_Labels.append(Label)

        return Label

    def AddLabel_spacer(self, Frame, Pack=False, Row=None, Column=None, CSpan=1, RSpan=1):
        '''
        Creates a Label for GUI spacing
        If pack is true then it will pack it into the given frame
        Row, Column etc. are not needed if packing is True
        '''
        # Creats the Label object
        Space_Label = TK.Label(Frame, bg=self.Background,
                               fg=self.Foreground, font=self.Font)

        # Packs the Label into the given master
        if Pack:
            Space_Label.pack(fill=TK.BOTH, expand=True)
        # Puts the Label into the grid
        else:
            Space_Label.grid(row=Row, column=Column,
                             sticky="nsew", columnspan=CSpan, rowspan=RSpan, padx=2, pady=2)

        self.All_Widgets.append(Space_Label)
        self.All_Labels.append(Space_Label)
        self.Space_Labels.append(Space_Label)

        return Space_Label

    def AddLabel_title(self, Frame, Text, Pack=False, Row=None, Column=None, CSpan=1, RSpan=1):
        '''
        Creates a Label for dispalaying titles
        If pack is true then it will pack it into the given frame
        Row, Column etc. are not needed if packing is True
        '''
        # Creats the Label object
        Title_Label = TK.Label(Frame, bg=self.Background,
                               fg=self.Foreground, font=self.TitleFont, text=Text)

        # Packs the Label into the given master
        if Pack:
            Title_Label.pack(fill=TK.BOTH, expand=True)
        # Puts the Label into the grid
        else:
            Title_Label.grid(row=Row, column=Column,
                             sticky="nsew", columnspan=CSpan, rowspan=RSpan, padx=2, pady=2)

        self.All_Widgets.append(Title_Label)
        self.All_Labels.append(Title_Label)
        self.Title_Labels.append(Title_Label)

        return Title_Label

    def AddLabel_subtitle(self, Frame, Text, Pack=False, Row=None, Column=None, CSpan=1, RSpan=1):
        '''
        Creates a Label for dispalaying sub titles
        If pack is true then it will pack it into the given frame
        Row, Column etc. are not needed if packing is True
        '''
        # Creats the Label object
        SubTitle_Label = TK.Label(Frame, bg=self.Background,
                                  fg=self.Foreground, font=self.SubTitleFont, text=Text)

        # Packs the Label into the given master
        if Pack:
            SubTitle_Label.pack(fill=TK.BOTH, expand=True)
        # Puts the Label into the grid
        else:
            SubTitle_Label.grid(row=Row, column=Column,
                                sticky="nsew", columnspan=CSpan, rowspan=RSpan, padx=2, pady=2)

        self.All_Widgets.append(SubTitle_Label)
        self.All_Labels.append(SubTitle_Label)
        self.SubTitle_Labels.append(SubTitle_Label)

        return SubTitle_Label

    def AddButton(self, Frame, Text, Pack=False, Row=None, Column=None, CSpan=1, RSpan=1):
        '''
        Adds a simple button to the Given frame
        If pack is true then it will pack it into the given frame
        Row, Column etc. are not needed if packing is True

        **You must add your own command using the .config method**
        '''
        Button = TK.Button(Frame, bg=self.Btn_Background, activebackground=self.Btn_Active,
                           foreground=self.Foreground, font=self.Font, text=Text)

        # Packs the Label into the given master
        if Pack:
            Button.pack(fill=TK.BOTH, expand=True)
        # Puts the Label into the grid
        else:
            Button.grid(row=Row, column=Column,
                        sticky="nsew", columnspan=CSpan, rowspan=RSpan, padx=2, pady=2)

        self.All_Widgets.append(Button)
        self.All_Buttons.append(Button)
        self.Pure_Buttons.append(Button)

        return Button

    def AddButton_negetive(self, Frame, Text, Pack=False, Row=None, Column=None, CSpan=1, RSpan=1):
        '''
        Adds a simple button to the Given frame
        If pack is true then it will pack it into the given frame
        Row, Column etc. are not needed if packing is True

        **You must add your own command using the .config method**
        **this only changes colours not functionality**
        '''
        Button_negetive = TK.Button(Frame, bg=self.QuitBtn_Background, activebackground=self.QuitBtn_Active,
                                    foreground=self.Foreground, font=self.Font, text=Text)

        # Packs the Label into the given master
        if Pack:
            Button_negetive.pack(fill=TK.BOTH, expand=True)
        # Puts the Label into the grid
        else:
            Button_negetive.grid(row=Row, column=Column,
                                 sticky="nsew", columnspan=CSpan, rowspan=RSpan, padx=2, pady=2)

        self.All_Widgets.append(Button_negetive)
        self.All_Buttons.append(Button_negetive)
        self.Negetive_Buttons.append(Button_negetive)

        return Button_negetive

    def AddButton_possitive(self, Frame, Text, Pack=False, Row=None, Column=None, CSpan=1, RSpan=1):
        '''
        Adds a simple button to the Given frame
        If pack is true then it will pack it into the given frame
        Row, Column etc. are not needed if packing is True

        **You must add your own command using the .config method**
        **this only changes colours not functionality of a normal button**
        '''
        Button_possitive = TK.Button(Frame, bg=self.PositiveBtn_Background, activebackground=self.PositiveBtn_Active,
                                     foreground=self.Foreground, font=self.Font, text=Text)

        # Packs the Label into the given master
        if Pack:
            Button_possitive.pack(fill=TK.BOTH, expand=True)
        # Puts the Label into the grid
        else:
            Button_possitive.grid(row=Row, column=Column,
                                  sticky="nsew", columnspan=CSpan, rowspan=RSpan, padx=2, pady=2)

        self.All_Widgets.append(Button_possitive)
        self.All_Buttons.append(Button_possitive)
        self.Positive_Buttons.append(Button_possitive)

        return Button_possitive

    def AddEntry(self, Frame, Focus=False, Show="", Pack=False, Row=None, Column=None, CSpan=1, RSpan=1):
        '''
        Creates an Entry object which lets the user enter text
        If pack is true then it will pack it into the given frame
        Row, Column etc. are not needed if packing is True
        '''
        Entry = TK.Entry(
            Frame, font=self.Font, foreground=self.Foreground)

        # If something else is ment to display not letters this will change
        if Show:
            Entry.config(show=Show)

        # Packs the Entry into the given master frame
        if Pack:
            Entry.pack(fill=TK.BOTH, expand=True)

        # Putes the entry into the grid
        else:
            Entry.grid(row=Row, column=Column, sticky="nsew",
                       padx=2, pady=2, columnspan=CSpan)

        # Focuses the users mouse onto the entry so they can just start typing
        if Focus:
            Entry.focus()

        self.All_Widgets.append(Entry)
        self.All_Entries.append(Entry)

        return Entry



# For debugging perpouses only
# This program is not designed to run by itself
if __name__ == "__main__":

    root = TK.Tk()

    TB = Basics(root, AccessType="database", Database="Test.db", Username="Ambrose")

    Main_fr = TB.AddFrame(root, Pack=True)

    Test_fr = TB.AddFrame(Main_fr, Row=0, Column=0)
    Test_fr.config(bg="red")

    Test_btn = TB.AddButton(Test_fr, "Tkig", Pack=True)

    TB.RemoveWidget(Test_btn)

    TB.Align_Grid_all()

    root.mainloop()

   
