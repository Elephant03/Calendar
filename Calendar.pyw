'''
The main python file that sets up the environment and calls the classes to build the GUI
'''

# Imports the needed packages
# For the gui
import tkinter
from Assets.Moduals.Utility import tkinter_basics
# For handleing the database
import sqlite3 as lite
# For loading the different year sections
from Assets.Moduals.Screens.YearDenominations import Year, Month, Week, Day
#from Year import Year_GUI
#from Month import Month_GUI
#from Week import Week_GUI
#from Day import Day_GUI
# Loads the start menu
from Assets.Moduals.Screens.StartMenu import StartMenu_GUI


class Main():
    '''
    The main class of the calendar aplication
    '''

    def __init__(self, root, Username="Default"):
        '''
        Sets up the main environement
        '''

        # Allows the root to be accessable
        self.root = root

        # Configures the root window
        self.root.wm_iconbitmap("./Assets/Images/calendar_icon.ico")
        self.root.title("Calendar")

        # Sets up a method for the tkinter basic
        self.TB = tkinter_basics.Basics(
            self.root, AccessType="database", Username=Username, Database="myDatabase.db")

        # Sets up the main frame
        self.Main_fr = self.TB.AddFrame(self.root, Pack=True)

        # Calls the start menu screen
        StartMenu_GUI.StartMenu(self)

        return


# Runs when the program is called directly and not if imported
if __name__ == "__main__":
    root = tkinter.Tk()

    Calendar = Main(root)

    root.mainloop()
